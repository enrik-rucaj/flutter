# am011_google_sign_in

Questo esercizio cerca di usare esclusivamente Visual STudio Code, cercheremo quindi, non usando Android Studio, di apportare le toppe opportune!

## Getting Started

Una volta creato un progetto **firebase** possiamo associare a tale progetto un applicazione Android. ANdando su `MainActivity,java` recuperiamo l'id del progetto

![img_001.png](img/img_001.png)

e quindi inseriamo, come spiegato [qui](https://developers.google.com/android/guides/client-auth) la stringa `SHA1`
```
keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore
```
di default la password è `android`, se si cambia macchina di sviluppo si deve rigenerare la stringa `SHA1`, procediano quindi generando il file `google-services.json`

![img_002.png](img/img_002.png)

che scaricheremo direttamente in `app/android`. Si tratta a questo punto di toccare `gradle` e qui arriva il difficile. 

## Google service plugin e firebase in gradle

Il riferimento è [qui](https://developers.google.com/android/guides/google-services-plugin), seguiamo le istruzioni (in modo da avere anche la versione aggiornata del plugin).
```
classpath 'com.google.gms:google-services:4.2.0'
```
va inserito nel file `build.gradle` *globale* che si trova nella radice del progetto. Quindi aggiungiamo **firebase** al nostro progetto android seguendo le istruzioni: [qui](https://firebase.google.com/docs/android/setup); ricordare di mettere il plugin in fondo
```
...
dependencies {
    ...
    implementation 'com.google.firebase:firebase-core:16.0.9'
}
apply plugin: 'com.google.gms.google-services'
```
e aggiungendo (per compatibilità coi pacchetti usate in flutter)
```
compileOptions {
         sourceCompatibility JavaVersion.VERSION_1_8
         targetCompatibility JavaVersion.VERSION_1_8
    }
```
Si veda anche

![img_003.png](./img/img_003.png)

lanciamo quindi l'app, ora ci siamo lasciati alle spalle i problemi. Potrebbe essere necessario la reinstallazione dell'app.

## flutter

Procediamo prima di tutto con l'integrazione di **firebase**: esso ci permetterà di accedere mediante account, qui scegliamo Google, ricordarsi di impostare l'accesso con account Google sulla console nel progetto)

![img_004.png](img/img_004.png)

per un'introduzio e a Firebase [qui](https://medium.com/flutterpub/flutter-how-to-do-user-login-with-firebase-a6af760b14d5); per prima cosa installiamo `firebase_auth`, [qui](https://github.com/flutter/plugins/tree/master/packages/firebase_auth) la sua home page; e il plugin per google, [qui](https://github.com/flutter/plugins/tree/master/packages/google_sign_in) l'home page, in `pubspec.yaml` apporteremo le opportune modifice:
```
...
dependencies:
  flutter:
    sdk: flutter

  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^0.1.2
  # flor flutter authentication
  firebase_auth: ^0.11.0
  # per il sign in con acount Google
  google_sign_in: ^4.0.1+3
...
```
A questo punto, apportate le modifiche va risolto un problema risolvibile aggiungendo su `gradle.properties` le righe seguenti
```
android.useAndroidX=true
android.enableJetifier=true
```
(risalvare i due `build.gradle` per sistemare); Android Studio ovviamente risolve questi problemi, viene infatti suggerito in rete di importare un tale progetto in Android Studio.

## Il codice

Usiamo i bottoni di autenticazione: [qui](https://github.com/dmjones/flutter_auth_buttons).
```
# flutter auth buttons
flutter_auth_buttons: ^0.5.0
```
