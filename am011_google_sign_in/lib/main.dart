import 'package:am011_google_sign_in/auth.dart';
import 'package:am011_google_sign_in/data_widget.dart';
import 'package:am011_google_sign_in/state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Google sign'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  AppState appState;

  @override
  void initState() {
    super.initState();
    appState = AppState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (!appState.isLogged) ? GoogleSignInButton(
              onPressed: (appState.isLoading == true) ? null : login, 
              darkMode: true, // default: false
            ) : Container(),
            (appState.isLogged) ? DataWidget (appState: appState, callback: logout) : Container()
          ],
        ),
      ),
    );
  }

  Future login() async {
    setState(() {
      appState.isLoading = true;
    });

    FirebaseUser firebaseUser = await loginWithGoogle().catchError((e) => setState(() {
      appState.isLoading = false;
    }));

    if(appState.user != firebaseUser) {
      setState(() {
        appState.isLoading = false;
        appState.user = firebaseUser;
        appState.isLogged = true;
        });
    }
  }

  // this will be passed to  the subWidget
  void logout() {
    setState(() {
      appState.isLoading = true;
    });

    
    logoutWithGoogle().whenComplete(() => setState(() {
      appState.isLoading = false;
      appState.user = null;
      appState.isLogged = false;
    })); 

  }
}

