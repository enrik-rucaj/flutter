import 'package:flutter/material.dart';

class MyGestureDetector extends StatelessWidget {
  
  final double xPos;
  final double yPos;
  final Function update;

  MyGestureDetector({Key key, this.xPos, this.yPos, this.update})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanDown: (DragDownDetails details) {
        RenderBox box = context.findRenderObject();
        Offset local = box.globalToLocal(details.globalPosition);
        update(local.dx, local.dy);
        print("pan down");
      },
      onPanUpdate: (DragUpdateDetails details) {
        RenderBox box = context.findRenderObject();
        Offset local = box.globalToLocal(details.globalPosition);
        update(local.dx, local.dy);
        print("pan update");
      },
      onPanEnd: (DragEndDetails details) {
        print("pan end");
      },
      child: Container(
        width: 350,
        height: 350,
        decoration: BoxDecoration(
            border: Border.all(
                color: Colors.orange, width: 4.0, style: BorderStyle.solid)),
        child: CustomPaint(
          painter: MyPaint(xPos, yPos),
          child: Text(
            "lp: (" +
                xPos.toStringAsFixed(2) +
                ", " +
                yPos.toStringAsFixed(2) +
                ")",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.cyan,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );
  }
}

class MyPaint extends CustomPainter {
  static const radius = 10.0;
  final double xPos;
  final double yPos;

  MyPaint(this.xPos, this.yPos);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = new Paint()
      ..color = Colors.orange
      ..style = PaintingStyle.fill;

    canvas.drawCircle(new Offset(xPos, yPos), radius, paint);
  }

  @override
  bool shouldRepaint(MyPaint old) => (xPos != old.xPos || yPos != old.yPos);
}
