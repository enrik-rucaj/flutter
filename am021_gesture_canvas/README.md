# am021_gesture_canvas

Questo esercizio sono: **gesture** con enfasi sull **disambiguation** per animare interattivamente gli oggetti -- figure -- di un **canvas**; per raggiungere tale obiettivo 
```
 @override
  bool shouldRepaint(MyPaint oldDelegate) 
    => (oldDelegate._gp.dx != _gp.dx || oldDelegate._gp.dy != _gp.dy);
```
fove `gp` si riferisce alla **global position** , [qui](https://flutter.dev/docs/development/ui/advanced/gestures#gesture-disambiguation), ancora, per un quadro d'insieme, in tal modo il canvas viene ridisegnato nel momento in cui il **ponter** -- solo uno in questo caso -- modifica òla sua posizione, la global posiztion. Abbiamo usato 
```
GestureDetector (
  onHorizontalDragStart: (DragStartDetails details) { 
    setState(() { ... });
  },
  onVerticalDragStart: (details) {
    setState(() { ... });
  },    
  onHorizontalDragUpdate: (DragUpdateDetails details) {
    setState(() { ... });
  },
  onVerticalDragUpdate: (details) {
    setState(() { ... });
  },
  onVerticalDragEnd: (DragEndDetails details) {
    setState(() { ... });
  },
  onHorizontalDragEnd: (detail) {
    setState(() { ... });
)
```  

## Esempi dal WEB

Ci sono interessanti esempi sul web:  
[1] "Flutter Canvas tutorial 05" di NieBin su Medium [qui](https://medium.com/flutteropen/canvas-tutorial-05-how-to-use-the-gesture-with-the-custom-painter-in-the-flutter-3fc4c2deca06).  
[2] "Flutter Custom Paint Tutorial Build a Radial Progress" su Medium [qui](https://medium.com/@rjstech/flutter-custom-paint-tutorial-build-a-radial-progress-6f80483494df).
