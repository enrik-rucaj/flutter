import 'package:am021_gesture_canvas/drawing.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'gesture and canvas',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'gesture and canvas'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Offset _gp = Offset(0.0, 0.0); // global position
  double _movex = 0;
  double _movey = 0;
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center (
        child: GestureDetector (
          onHorizontalDragStart: (DragStartDetails details) {
            setState(() {
              _gp = details.globalPosition;
              _gp = details.globalPosition;
              }
            );
          },
          onVerticalDragStart: (details) {
            setState(() {
              _gp = details.globalPosition;
              print("_gp: " + _gp.toString());
            });
          },
          // look at next two events
          onHorizontalDragUpdate: (DragUpdateDetails details) {
            setState(() {
              _gp = details.globalPosition; // try to remove it
              _movex += details.delta.dx;
              print("movex: " + _movex.toString());
              }
            );
          },
          onVerticalDragUpdate: (details) {
            setState(() {
              _gp = details.globalPosition;
              _movey += details.delta.dy;
              print("movey: " + _movey.toString());
            });
          },
          /*
          onVerticalDragEnd: (DragEndDetails details) {
            setState(() {
              _gp = details.globalPosition;
              }
            );
          },
          onHorizontalDragEnd: (detail) {
            setState(() {
              _gp = details.globalPosition;
              }
            );
          },
          */
          child: Container(
            width: 350,
            height: 350,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.orange,
                width: 4.0,
                style: BorderStyle.solid
                )
            ),
            child: CustomPaint(
              painter: MyPaint(_movex, _movey, _gp),
              child: Text(
                "gp: " + _gp.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold, 
                  color: Colors.cyan, 
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
        )
      )
    );
  }
}
