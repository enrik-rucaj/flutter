import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am007_drag_and_drop'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  static Random _rnd = new Random();
  int _number = _rnd.nextInt(100);

  void _refresh() {
    setState(() {
      _number = _rnd.nextInt(100);
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            DraggableNumber(number : _number),
             Row (
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MyTarget(notifyParent: _refresh, isEven: true),
                MyTarget(notifyParent: _refresh, isEven: false),
              ]
             )
          ],
        ),
      ),
    );
  }
}

class DraggableNumber extends StatelessWidget {

  DraggableNumber({Key key, this.number}) : super(key: key);

  final int number;

  @override
  Widget build(BuildContext context) {
    return Draggable(
      data: number, 
      child: Container(
        width: 100.0,
        height: 100.0,
        child: Center(
          child: Text(
            '$number',
            style: TextStyle(color: Colors.white, fontSize: 22.0),
          ),
        ),
        color: Colors.pink,
      ),
      childWhenDragging: Container(
        width: 100,
        height: 100,
      ),
      feedback: Container(
        width: 100.0,
        height: 100.0,
        child: Center(
        child: Text(
        '$number',
        style: TextStyle(color: Colors.white, fontSize: 22.0),
        ),
      ),
      color: Colors.pink[200],
      ),
    );
  }
}

class MyTarget extends StatelessWidget {

  MyTarget({Key key, this.notifyParent, this.isEven}) : super(key: key);

  final bool isEven;
  final Function() notifyParent; // to propagate state of parent
  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0,
      height: 100.0,
      color: isEven ? Colors.green : Colors.deepPurple,
      child: DragTarget(
        builder: (BuildContext context, List<int> candidateData, List rejectedData) {
          if(isEven)
            return Center(child: Text("Even", style: TextStyle(color: Colors.white, fontSize: 22.0),));
          else
            return Center(child: Text("Odd", style: TextStyle(color: Colors.white, fontSize: 22.0),));
        },
        onWillAccept: (data) {
          return true;
        },
        onAccept: (data) {
          if(isEven && data % 2 == 0 || !isEven && data % 2 != 0){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Correct!")));
            notifyParent();
          }
          else {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Wrong!")));
          } 
        },
      )
    );
  }  
}


  