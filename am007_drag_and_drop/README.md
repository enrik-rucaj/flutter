# am007_drag_and_drop

Da Medium invitiamo alla lettura di 
"A Deep Dive Into Draggable and DragTarget in Flutter" di D.Joshi [qui](https://medium.com/flutter-community/a-deep-dive-into-draggable-and-dragtarget-in-flutter-487919f6f1e4)

## draggable
`Draggable` [qui](https://api.flutter.dev/flutter/widgets/Draggable-class.html). La gestione del `Draggable` prevede
- `child` il *widget* da visualizzare quando nessun *drag*  attivo.
- `feedbach` è il *widget* che viene mostrato sotto il puntatore del mouse a drag attivo/i.
- `childWhenDragging` indica il *widget* mostrato, nel posto originale, quando il *drag* è attivo.
- `data`  è quanto viene passato tramite l'operazione di *drag and drop*.
-  `childWhenDragging` per la classe indica il massimo numero di istanza che possono avere il *drag*, ponendolo a zero si può inibire il *drag*.


