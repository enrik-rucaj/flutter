# am015_hero

Per gestire la velocità dell'animazione usiamo
```
import 'package:flutter/scheduler.dart' show timeDilation;
```
e
```
timeDilation = 1.0; // 1.0 means normal animation speed.
```
per le API vedi [qui](https://api.flutter.dev/flutter/scheduler/scheduler-library.html). L'esercizio è molto simile ad uno dei primi già visti. Il cuore di tutto è
```
class MyHero extends StatelessWidget {
  
  const MyHero({ Key key, this.photo, this.onTap, this.width }) : super(key: key);

  final String photo;
  final Function onTap;
  final double width;

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Image.asset(
              photo,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}
```
Per le API di `Material` vedi [qui](https://api.flutter.dev/flutter/material/Material-class.html).