import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am030_compute',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am030_compute'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  int _done = 0;
  String _msg;

  void _startLongCalculation(String msg) async {
    setState(() {
      _done = 1;
    });
    String ret = await compute(longRunner, msg);
    setState(() {
      _msg = ret;
      _done = 2;
    });
  }

  Widget _display(String msg, int done) {
    if (done == 1) return CircularProgressIndicator();
    else if (done == 0) return Text('', style: Theme.of(context).textTheme.display1);
    else return Text(msg, style: Theme.of(context).textTheme.display1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'type a message',
                ),
              onSubmitted: _startLongCalculation
              ),
              Container(
                padding:EdgeInsets.all(40.0),
                child: _display(_msg, _done),                
              )
          ],
        ),
      ),
    );
  }
}

Future<String> longRunner(String msg) async {
  var rgn = Random();
  int sec = 3 + rgn.nextInt(8);
  await Future.delayed(Duration(seconds: sec));
  return 'after $sec seconds:\n$msg';
}


