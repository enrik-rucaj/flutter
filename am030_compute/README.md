# am030_compute

Qui sperimentiamo un worker *isolate*, nella documentazione troviamo questo interessante [caso](https://flutter.dev/docs/cookbook/networking/background-parsing) (di cui abbiamo gatto cenno). Uno sguardo alle api [qui](https://api.flutter.dev/flutter/foundation/compute.html) che ci permette in modo comodo di fare lo *spawn* di un *isolate* 
``` dart
Future<String> longRunner(String msg) async {
  var rgn = Random();
  int sec = 3 + rgn.nextInt(8);
  await Future.delayed(Duration(seconds: sec));
  return 'after $sec seconds:\n$msg';
}
```
è il nostro lungo calcolo! Per eseguirlo su di un *worker isolate* abbiamo proceduto così
``` dart
void _startLongCalculation(String msg) async {
    ...
    String ret = await compute(longRunner, msg);
    ...
  }
  ```