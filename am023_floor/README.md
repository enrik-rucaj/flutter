# am023_floor

A new Flutter project.

## DAO: Data Object Interface

DAO, vedi [qui](https://en.wikipedia.org/wiki/Data_access_object) (Wikipedia: en) definisce l'**interfaccia** per l'accesso ai dati.  Per le API leggere [qui](https://pub.dev/documentation/floor/latest/#persisting-data-changes) con attenzione!
``` dart
@dao
abstract class TaskDao {
  @Query('SELECT * FROM task WHERE id = :id')
  Future<Task> findTaskById(int id);

  @Query('SELECT * FROM task')
  Future<List<Task>> findAllTasks();

  @Query('SELECT * FROM task')
  Stream<List<Task>> findAllTasksAsStream();

  @insert
  Future<void> insertTask(Task task);

  @insert
  Future<void> insertTasks(List<Task> tasks);

  @update
  Future<void> updateTask(Task task);

  @update
  Future<void> updateTasks(List<Task> task);

  @delete
  Future<void> deleteTask(Task task);

  @delete
  Future<void> deleteTasks(List<Task> tasks);
}
```

## il database

Nel file `database.dart` abbiamo
``` dart
import 'dart:async';

import 'package:floor/floor.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'task.dart';
import 'task_dao.dart';

part 'database.g.dart';

@Database(version: 1, entities: [Task])
abstract class FlutterDatabase extends FloorDatabase {
  TaskDao get taskDao;
}
```
All'inizio `database.g.dart` non esiste, ecco l'errore (ugnorare i warning): dare **da terminale** quanto segue
```
flutter packages pub run build_runner build
```
Per noi, abendo
```
dev_dependencies:
  flutter_test:
    sdk: flutter
  floor_generator: ^0.10.0
  build_runner: ^1.7.3
```
viene generato il file `database.g.dart` (`g` sta per generated).

## dismissible

Per una guida vedi [qui](https://flutter.dev/docs/cookbook/gestures/dismissible). Ogni `Widget` viene identificata da una proprietà `key` [qui](https://api.flutter.dev/flutter/widgets/Widget/key.html)  di tipo `Key` [qui](https://api.flutter.dev/flutter/foundation/Key-class.html), in questo caso sarebbe sufficiente una `LocalKey`. Un `Widgt` che andrà a "posizionarsi" nello stesso `Widget` deve avere 
lo stesso `Key` (vedi gli `Elements` generati); ad ogni `Object` viene associato un `hashCode` e l'operatore di confronto `==` fra `Objecte` prevede il confronto fra tali `hashCode`, vedi [qui](https://api.flutter.dev/flutter/dart-core/Object/hashCode.html). Una `Key`, meglio una `LocalKey`, viene utilizzata da `Dismissible` per riposizionare nello stesso posto del `Widget`, con opportuna animazione, il `background`, ecco il motivo per cui diamo l'override dell'operator `==` [qui](https://api.flutter.dev/flutter/widgets/Element/operator_equals.html)
``` dart
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Task &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              message == other.message;
```
in `widgets.dart`
``` dart
...
 return Dismissible(
      key: Key('${task.hashCode}'),
      background: Container(
        color: Colors.red[900]
...
```
In alternativa si può usare la classe `UniqueKey` per generare chiavi non ripetibili.


## main

Per far funzionare il tutto l'esempio propone questa soluzione che poi noi andremo a rivedere nel prossimo esempio
``` dart
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final database = await $FloorAppDatabase
      .databaseBuilder('flutter_database.db')
      .build();
  final dao = database.taskDao;

  runApp(FloorApp(dao));
}
```
Il *trick* consiste nell'usare 
``` dart
WidgetsFlutterBinding.ensureInitialized();
```
per le API [qui](https://api.flutter.dev/flutter/widgets/WidgetsFlutterBinding-class.html).  La chiamata del metodo `ensureInitialized` [qui](https://api.flutter.dev/flutter/widgets/WidgetsFlutterBinding/ensureInitialized.html) evita errori dovuti alla scelta dell'uso di *widget asincrone*.
> You only need to call this method if you need the binding to be initialized before calling runApp.

## il refresh asincrono della lista

Nel momento in cui la base di dati SQLite ([qui](https://www.sqlite.org/index.html)) viene aggiornata viene generato un evento
``` dart
@Query('SELECT * FROM task')
Stream<List<Task>> findAllTasksAsStream();
```
e ricostruita la lista, per far ciò utilizziamo un *widget asincrono* ([qui](https://flutter.dev/docs/development/ui/widgets/async)): usiamo infatti uno `StreamBuilder` [quui](https://api.flutter.dev/flutter/widgets/StreamBuilder-class.html), esso prevede due importanti *property*
``` dart
StreamBuilder<int>(
  stream: _asyncComputation, 
  builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
```
dove `_asyncComputation` è una computazione asincrona il cui risultato è di tipo `T`, il risultato (sempre di tipo `T`) viene consegnato, se tutto va a buon fine, in
``` dart
snapsho.result
```
