import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Grid'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: GridView.count(
          crossAxisCount: 2, // colonne
          padding: const EdgeInsets.all(10.0), // for the grid content
          children: List.generate(50, (index) {
            return Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                color: Utility.getColor(),
                child: Center(
                  child: Text('$index', style: TextStyle(fontSize: 40.0,)),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}

class Utility {

  static Random rnd = new Random();
  static int index = 0;
  static final List<Color> colors = [Colors.red, Colors.yellow, Colors.green, Colors.blue, Colors.brown];

  static Color getRandomColor() {    
    return colors[rnd.nextInt(5)];
  }

  static Color getColor() {
    return colors[index++ % 5];
  }
  
}

