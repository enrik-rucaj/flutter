# am026_websocket

Il progetto è praticamente quello proposto in documentazione: [qui](https://flutter.dev/docs/cookbook/networking/web-sockets) che fa uso di `web_socket_channel 1.0.14`  [qui](https://pub.dev/packages/web_socket_channel#-installing-tab-).

## Il nostro server

Questo è il sorgente del nostro server in dart, un echo srver
```
import 'dart:io';

void main() {
  HttpServer.bind(InternetAddress.anyIPv4, 8080).then((HttpServer server) {
    print("HttpServer listening...");
    server.serverHeader = "an echo server";
    server.listen((HttpRequest request) {
      // Checks whether the request is a valid WebSocket upgrade request
      if (WebSocketTransformer.isUpgradeRequest(request)){
        WebSocketTransformer.upgrade(request).then(handleWebSocket);
      }
      else {
        print("Regular ${request.method} request for: ${request.uri.path}");
        serveRequest(request);
      }
    });
  });
}

void handleWebSocket(WebSocket socket){
  print('Client connected!');
  socket.listen((data) {
    String msg = data.toString();
    print('Client sent: $msg');
    socket.add('echo: $msg');
  },
  onDone: () {
    print('Client disconnected');  
  });
}

void serveRequest(HttpRequest request){
  request.response.statusCode = HttpStatus.forbidden;
  request.response.reasonPhrase = "WebSocket connections only";
  request.response.close();
}
```