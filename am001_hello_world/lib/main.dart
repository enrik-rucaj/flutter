import 'package:flutter/material.dart';

void main() {
  runApp(
    Center(
      child: Text(
        'Hello World',
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        textDirection: TextDirection.ltr,
        style: TextStyle(fontSize: 50.0, color: Colors.yellow)
      )
    )
  );
}