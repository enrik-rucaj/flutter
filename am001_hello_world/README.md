# am001_hello_world

Il nostro primo progetto; quanto segue ci viene proposto dalle API
```
 void runApp (Widget app)
```
realizza l'**inflate** attaccandola allo scfermo (screen), vedi [qui](https://api.flutter.dev//flutter/widgets/runApp.html). `Center` è un widget che sistema al centro il suo contenuto ovvero la **property** `child` che a sua voltaè un text
```
child: Text(
        'Hello World',
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        textDirection: TextDirection.ltr,
        style: TextStyle(fontSize: 50.0, color: Colors.yellow)
      )
```
ovvero un testo: `Colors` fa riferimento alle **material design specifications**. 

