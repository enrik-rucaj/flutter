import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Passing Arguments Again',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {


  final List data = <String>["uno", "due", "tre", "quattro"];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Item(number: data[0]),
            Item(number: data[1]),
            Item(number: data[2]),
            Item2(data[3]),
          ],
        ),
      ),
    );
  }
}

class Item extends StatelessWidget {

  final String number;

  const Item({Key key, @required this.number}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
          number,
          style: TextStyle(fontSize: 40)
      ),
    );
  }
}

class Item2 extends StatelessWidget {

  final String number;

  Item2(this.number){}

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
          number,
          style: TextStyle(fontSize: 40)
      ),
    );
  }
}