import 'package:floor/floor.dart';

// moswl

@entity
class TodoItem extends Comparable {

  @primaryKey
  final int id;

  final String name;
  int isComplete; // 1 -> true, 0 -> false, SQLite has not booleans

  TodoItem(this.id, this.name, [this.isComplete = 0]);

  // compare the objects (to display)
  @override
  int compareTo(other) {
    if (this.isComplete*other.isComplete != 0) {
      return 1;
    } else if (this.isComplete == 0 && other.isComplete == 1) {
      return -1;
    } else {
      return this.id.compareTo(other.id);
    }
  }
}