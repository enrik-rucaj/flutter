// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? join(await sqflite.getDatabasesPath(), name)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  TodoItemDao _todoItemDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    return sqflite.openDatabase(
      path,
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TodoItem` (`id` INTEGER, `name` TEXT, `isComplete` INTEGER, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
  }

  @override
  TodoItemDao get todoItemDao {
    return _todoItemDaoInstance ??= _$TodoItemDao(database, changeListener);
  }
}

class _$TodoItemDao extends TodoItemDao {
  _$TodoItemDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _todoItemInsertionAdapter = InsertionAdapter(
            database,
            'TodoItem',
            (TodoItem item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'isComplete': item.isComplete
                }),
        _todoItemUpdateAdapter = UpdateAdapter(
            database,
            'TodoItem',
            ['id'],
            (TodoItem item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'isComplete': item.isComplete
                }),
        _todoItemDeletionAdapter = DeletionAdapter(
            database,
            'TodoItem',
            ['id'],
            (TodoItem item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'isComplete': item.isComplete
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _todoItemMapper = (Map<String, dynamic> row) => TodoItem(
      row['id'] as int, row['name'] as String, row['isComplete'] as int);

  final InsertionAdapter<TodoItem> _todoItemInsertionAdapter;

  final UpdateAdapter<TodoItem> _todoItemUpdateAdapter;

  final DeletionAdapter<TodoItem> _todoItemDeletionAdapter;

  @override
  Future<List<TodoItem>> getTodoItems() async {
    return _queryAdapter.queryList('SELECT * FROM TodoItem',
        mapper: _todoItemMapper);
  }

  @override
  Future<TodoItem> findTodoItemById(int id) async {
    return _queryAdapter.query('SELECT * FROM TodoItem WHERE id = ?',
        arguments: <dynamic>[id], mapper: _todoItemMapper);
  }

  @override
  Future<void> insertTodoItem(TodoItem item) async {
    await _todoItemInsertionAdapter.insert(
        item, sqflite.ConflictAlgorithm.abort);
  }

  @override
  Future<void> insertTodoItems(List<TodoItem> items) async {
    await _todoItemInsertionAdapter.insertList(
        items, sqflite.ConflictAlgorithm.abort);
  }

  @override
  Future<void> updateTodoItem(TodoItem item) async {
    await _todoItemUpdateAdapter.update(item, sqflite.ConflictAlgorithm.abort);
  }

  @override
  Future<void> updateTodoItems(List<TodoItem> items) async {
    await _todoItemUpdateAdapter.updateList(
        items, sqflite.ConflictAlgorithm.abort);
  }

  @override
  Future<void> deleteTodoItem(TodoItem items) async {
    await _todoItemDeletionAdapter.delete(items);
  }

  @override
  Future<void> deleteTodoItems(List<TodoItem> itemss) async {
    await _todoItemDeletionAdapter.deleteList(itemss);
  }
}
