import 'package:flutter/material.dart';

import 'database.dart';
import 'todo_item.dart';
import 'todo_item_dao.dart';


// TodoListScreen

class TodoListScreen extends StatefulWidget {
  TodoListScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TodoListScreenState createState() => new _TodoListScreenState();

}

class _TodoListScreenState extends State<TodoListScreen> {

  TodoItemDao dao;

  List<TodoItem> _todoItems = List();
  
  
  /* NO
  _TodoListScreenState() {
    database = await ...
  }
  */

  @override
  initState() {
    super.initState();
    _getDao();
  }

  Future<Null> _getDao() async {
    AppDatabase database = await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    dao = database.todoItemDao;
    _updateTodoItems();
  }

  // a service method
  _updateTodoItems() {
    dao.getTodoItems().then((items) {
      setState(() { _todoItems = items; });
    });
  }

  void _addTodoItem() async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) => AddTodoItemScreen(dao)));
    _updateTodoItems();
  }

  Widget _createTodoItemWidget(TodoItem item) {
    return ListTile(
      title: Text(item.name),
      contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
        trailing: Checkbox(
          value: item.isComplete == 1,
          onChanged: (value) => _updateTodoCompleteStatus(item, value),
      ),
      onLongPress: () => _displayDeleteConfirmationDialog(item),
    );
  }

  void _updateTodoCompleteStatus(TodoItem item, bool newStatus) {
    item.isComplete = newStatus ? 1 : 0;
    dao.updateTodoItem(item);
    _updateTodoItems();
  }

  void _deleteTodoItem(TodoItem item) {
    dao.deleteTodoItem(item);
    _updateTodoItems();
  }

  // delete dialog

  Future<Null> _displayDeleteConfirmationDialog(TodoItem item) {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true, // Allow dismiss when tapping away from dialog
      builder: (BuildContext context) {
        return  AlertDialog(
          title: Text("Delete TODO"),
          content: Text("Do you want to delete \"${item.name}\"?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
             onPressed: Navigator.of(context).pop, // Close dialog
            ),
            FlatButton(
              child: Text("Delete"),
              onPressed: () {
                _deleteTodoItem(item);
                Navigator.of(context).pop(); // Close dialog
              },
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    _todoItems.sort();
    // list = iterable -> iterable (via map) -> list
    final todoItemWidgets = _todoItems.map(_createTodoItemWidget).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: todoItemWidgets,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addTodoItem,
        tooltip: 'Add Todo',
        child: Icon(Icons.add),
      ),
    );
  }
}

// AddTodoUtem

class AddTodoItemScreen extends StatefulWidget {

  AddTodoItemScreen(this.dao);

  TodoItemDao dao;

  @override
  State<StatefulWidget> createState() => new _AddTodoItemScreenState();
}

class _AddTodoItemScreenState extends State<AddTodoItemScreen> {

  final _todoNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Add Todo Item")),
        body: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: "Todo Name"),
                controller: _todoNameController,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text("Cancel"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  RaisedButton(
                    child: Text("Save"),
                    onPressed: () {
                      TodoItem item = TodoItem(null, _todoNameController.text);
                      widget.dao.insertTodoItem(item);
                      Navigator.pop(context);
                    },
                  )
                ],
              )
            ],
          )
        )
      );
  }

  @override
  void dispose() {
    _todoNameController.dispose();
    super.dispose();
  }
}