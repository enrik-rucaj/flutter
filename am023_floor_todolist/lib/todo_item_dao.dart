import 'package:floor/floor.dart';

import 'todo_item.dart';

@dao
abstract class TodoItemDao {
  @Query('SELECT * FROM TodoItem')
  Future<List<TodoItem>> getTodoItems();

  @Query('SELECT * FROM TodoItem WHERE id = :id')
  Future<TodoItem> findTodoItemById(int id);

  @insert
  Future<void> insertTodoItem(TodoItem item);

  @insert
  Future<void> insertTodoItems(List<TodoItem> items);

  @update
  Future<void> updateTodoItem(TodoItem item);

  @update
  Future<void> updateTodoItems(List<TodoItem> items);

  @delete
  Future<void> deleteTodoItem(TodoItem items);

  @delete
  Future<void> deleteTodoItems(List<TodoItem> itemss);
}


   
 