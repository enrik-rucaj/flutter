# am023_todo_list_floor

Questo esempio è il rifacimento di **am006_to_do_list** usando `floor` , [qui](https://github.com/vitusortner/floor) per la documentazione, che vuole imitare `Room` per Android.

## requisiti

```
version: 1.0.0+1

environment:
  sdk: ">=2.1.0 <3.0.0"

dependencies:
  flutter:
    sdk: flutter
  floor: ^0.10.0

  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^0.1.2

dev_dependencies:
  floor_generator: ^0.10.0 
  build_runner: ^1.7.3
  flutter_test:
    sdk: flutter
```

## model

A differenza dell'esempio da cui siamo partiti qui il model segue un approccio più  in mod da rispettare i tipi di dati di SQLite, non abbiamo usato il `bool` non presente.
```
@entity
class TodoItem extends Comparable {

  @primaryKey
  final int id;

  final String name;
  int isComplete; // 1 -> true, 0 -> false, SQLite has not booleans

  TodoItem(this.id, this.name, [this.isComplete = 0]);

  // compare the objects (to display)
  @override
  int compareTo(other) {
    if (this.isComplete*other.isComplete != 0) {
      return 1;
    } else if (this.isComplete == 0 && other.isComplete == 1) {
      return -1;
    } else {
      return this.id.compareTo(other.id);
    }
  }
}
```
Si vedono subito le **annotation** proprio come in Room! Oltre al model definiamo il **dao** (Data Access Object)
```
@dao
abstract class TodoItemDao {
  @Query('SELECT * FROM TodoItem')
  Future<List<TodoItem>> getTodoItems();

  @Query('SELECT * FROM TodoItem WHERE id = :id')
  Future<TodoItem> findTodoItemById(int id);

  @insert
  Future<void> insertTodoItem(TodoItem item);

  @insert
  Future<void> insertTodoItems(List<TodoItem> items);

  @update
  Future<void> updateTodoItem(TodoItem item);

  @update
  Future<void> updateTodoItems(List<TodoItem> items);

  @delete
  Future<void> deleteTodoItem(TodoItem items);

  @delete
  Future<void> deleteTodoItems(List<TodoItem> itemss);
 }
```
ovvero l'interfaccia. Resta l'ultima interfaccia
```
part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [TodoItem])
abstract class AppDatabase extends FloorDatabase {
  TodoItemDao get todoItemDao;
}
```
per completare il tutto, esssa si trova nel file `database.dart` e grazie al comando
```
flutter packages pub run build_runner build
```
(qui entra in gioco `build_runner`) otteniamo automaticamente il codice nel file `database.g.dart` (`g` sta per generated).
```
flutter packages pub run build_runner clean
```
permette di cancellare tale file.

