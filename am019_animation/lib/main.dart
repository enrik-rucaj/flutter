import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

import 'cross_painter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'canvas animation',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {

  double _fraction = 0.0;
  Animation<double> _animation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0), 
              width: 120.0,
              height: 80.0,
              child: Text( 
                "The cross:",
                style: new TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold
                  ),
                ),
              ),
            Container(
              padding: EdgeInsets.all(10.0), 
              width: 170.0,
              height: 170.0,
              child: CustomPaint(
                painter: CrossPainter(_fraction)
              )
            )
          ]
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    AnimationController controller = AnimationController(
      duration: Duration(milliseconds: 1000), vsync: this);

    // Tween is a linear interpolation between a beginning and ending value
    _animation = Tween(begin: 0.0, end: 1.0).animate(controller)
    ..addListener(() {
      setState(() {
        _fraction = _animation.value;
        });
    });
    // start animation
    controller.forward();
  }

}