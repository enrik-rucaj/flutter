# am019_canvas_animation

Questo breve esempio, tratto da [qui](http://myhexaville.com/2018/04/30/flutter-canvas-animations/), unisce le animazioni al disegno su `canvas`. 

## I step: un painter parametrico

Il primo passo è quello di costruire un `CustomPainter` parametrico che, al variare del paramatro, venga ridisegnato:
``` dart
@override
bool shouldRepaint(CrossPainter oldDelegate) {
  return oldDelegate._fraction != _fraction;
}
```
Lo stato è costituito da `fraction`.

## II step: l'oggetto animation

Per prima cosa prepariamo il motore
``` dart
AnimationController controller = AnimationController(
      duration: Duration(milliseconds: 1000), vsync: this);
```
Lo stato del nostro `StatefullWidget` infatti è dichiarato come
``` dart
class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin
```
e
> An AnimationController needs a TickerProvider, which is configured using the vsync argument on the constructor.

vedi [qui](https://api.flutter.dev/flutter/animation/AnimationController-class.html). Un *ticker* è in sostanza un *trigger* che gestisce l'avanzamento dell'animazione.
>  The TickerProviderStateMixin class always works for this purpose; the SingleTickerProviderStateMixin is slightly more efficient in the case of the class only ever needing one Ticker (e.g. if the class creates only a single AnimationController during its entire lifetime).

Con un `AnimationController` possiamo creare un `Animation`
``` dart
_animation = Tween<double>(begin: 0.0, end: 1.0).animate(controller)
```

## III step: unire il tutto

Col motore possiamo gestire l'avanzamento del parametro
``` dart
..addListener(() {
      setState(() {
        _fraction = _animation.value;
        });
    });
```
