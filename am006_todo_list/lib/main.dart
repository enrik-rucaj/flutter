import 'package:am006_todo_list/tdoListScreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To Do List',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home:  TodoListScreen(title: 'Todo List'),
    );
  }
}

