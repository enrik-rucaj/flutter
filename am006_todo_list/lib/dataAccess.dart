import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'todoItem.dart';



class DataAccess {
  
  static final DataAccess _instance = DataAccess._privateConstructor();
  final String todoTable = "TodoItems";
  
  Database _db;

  DataAccess._privateConstructor();

  factory DataAccess() {
    return _instance;
  }

  Future<void> open() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "my.db");

    _db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
            CREATE TABLE $todoTable ( 
            id INTEGER PRIMARY KEY AUTOINCREMENT, 
            name TEXT NOT NULL,
            isComplete INTEFER NOT NULL ) 
            ''');
    });

    // default initialization
    if((await getTodoItems()).length == 0) {      
      insertTodo(TodoItem(name: "Comprare bagigi", isComplete: true));
      insertTodo(TodoItem(name: "Comprare seme col sal"));
      insertTodo(TodoItem(name: "Verzer a taverna"));
    }
  }

  Future<List<TodoItem>> getTodoItems() async {
    var data = await _db.query(todoTable);
    return data.map((d) => TodoItem.fromMap(d)).toList();
  }
  
  Future<int> insertTodo(TodoItem item) {
    return _db.insert(todoTable, item.toMap());
  }

  Future<int> updateTodo(TodoItem item) {
    return _db.update(todoTable, item.toMap(),
    where: "id = ?", whereArgs: [item.id]);
  }
  Future deleteTodo(TodoItem item) {
    return _db.delete(todoTable, where: "id = ?", whereArgs: [item.id]);
  }

}