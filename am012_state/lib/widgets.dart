import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyStateWidget extends StatefulWidget {

  const MyStateWidget({Key key }) : super(key: key);

  @override
  MyState createState() => MyState();

  static MyState of(BuildContext context) {
    assert(context != null);
    final MyState result = context.findAncestorStateOfType<MyState>();
    if (result != null) 
      return result;
    throw FlutterError("MyState error");
  }
}

class MyState extends State<MyStateWidget> {

  Color color = Colors.orange;

  @override
  void initState() {
    super.initState();
    color = Colors.green;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            color: color,
            constraints: BoxConstraints(
              maxHeight: 180.0,
              maxWidth: 180.0,
              minWidth: 180.0,
              minHeight: 180.0
            ),
          ),
          SizedBox(height: 20), // a padding trick
          MyWidget()
        ],
      ),
    );
  }

  // update state
  void updateState() {
    Color newColor;
    if(color == Colors.green)
      newColor = Colors.blue;
    else if (color == Colors.blue)
      newColor = Colors.yellow;
    else if (color == Colors.yellow)
      newColor = Colors.green;
    setState(() {
      color = newColor;
    });
  }
  
}

class MyWidget extends StatelessWidget {
  const MyWidget({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      // a trick to pass an argument to onPressed action ...
      onPressed: () => swapColor(context),
      child: Text(
        'Change Color',
        style: TextStyle(fontSize: 20)
      ),
    );
  }

  void swapColor(BuildContext context) {
    MyState state = MyStateWidget.of(context);  
    state.updateState();
  }
}