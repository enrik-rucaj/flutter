# am011_state

In altri esercizi abbiamo usato una sorta di *pattern* di callback, qui, seguendo la definzione per `Scaffold` del metodo `of`, per accedere allo stato abbiamo creato il metodo
``` dart
static MyState of(BuildContext context) {
    assert(context != null);
    final MyState result = context.findAncestorStateOfType<MyState>();
    if (result != null) 
      return result;
    throw FlutterError("MyState error");
}
```
