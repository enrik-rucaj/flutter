import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sensors/sensors.dart';

import 'data.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am029_maree',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: 'am029_maree'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _stazione;
  String _data;
  String _valore;
  StreamSubscription _userAccelerometerSub;
  
  @override
  void initState() {
    _userAccelerometerSub =  userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      // vertical gesture
      setState(() { 
        if(event.z.abs() > 2) _getData();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _userAccelerometerSub.cancel();
    super.dispose();
  }

  void _getData() async {
    List<Data> dataList = await fetchDataList();
    setState(() {
      _stazione = dataList[0].stazione;
      _data = dataList[0].data;
      _valore = dataList[0].valore;
    });
  }

  Color _getColor(){
    if (_valore == null) return Colors.black;
    double valore = double.parse(_valore.split(' ')[0]);
    if (valore < 0.80){ 
      return Colors.green; 
    } else if (valore < 0.1) { 
      return Colors.yellow; 
    } else if (valore < 0.120) { 
      return Colors.orange; 
    } else if (valore < 0.180) { 
      return Colors.red; q
    } else {
      return Colors.purple;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Stazione: ${_stazione ?? ' '}, \ndata: ${_data ?? ' ' }',
            ),
            Text(
              'marea: ${_valore ?? ' '}',
              style: TextStyle(
                fontSize: 40,
                color: _getColor(),
                ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getData,
        tooltip: 'Get Data',
        child: Icon(Icons.arrow_downward),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


