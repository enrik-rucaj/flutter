import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am027 Dialogs'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _text = "nulla per ora";

  void _setText(String text) {
    setState(() {
      _text = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(20),
              child: Text(
                'Chose your dialogs',
                style: TextStyle(color: Colors.grey[800], fontSize: 30),
              ),
            ),
            RaisedButton(
              onPressed: () {
                _ackAlert(context, this);
              },
              child: const Text("Alert Dialog"),
            ),
            RaisedButton(
              onPressed: () async {
                final ConfirmAction action =
                    await _asyncConfirmDialog(context);
                print("Confirm Action $action");
                _setText("Confirm Action $action");
              },
              child: const Text("Confirm Dialog"),
            ),
            RaisedButton(
              onPressed: () async {
                final Spriz spriz = await _asyncSimpleDialog(context);
                print("your drink: $spriz");
                _setText("your drink: $spriz");
              },
              child: const Text("Simple dialog"),
            ),
            RaisedButton(
              onPressed: () async {
                final String myDrink =
                    await _asyncInputDialog(context);
                print("My drink is is $myDrink");
                _setText("My drink is is $myDrink");
              },
              child: const Text("Input Dialog"),
            ),
            Container(margin: EdgeInsets.all(20), child: Text('$_text'))
          ],
        ),
      ),
    );
  }
}

// for a simple alert dialog

Future<void> _ackAlert(BuildContext context, _MyHomePageState state) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('A simple alert'),
        content: const Text('Dismiss this alert amd ...'),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
              state._setText("alert dismissed");
            },
          ),
        ],
      );
    },
  );
}

// for a simple confirm dialog

enum ConfirmAction { CANCEL, ACCEPT }

Future<ConfirmAction> _asyncConfirmDialog(
    BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Chose an option'),
        content: const Text('Try to chose an option ...'),
        actions: <Widget>[
          FlatButton(
            child: const Text('CANCEL'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.CANCEL);
            },
          ),
          FlatButton(
            child: const Text('ACCEPT'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.ACCEPT);
            },
          )
        ],
      );
    },
  );
}

// for an async simple dialog: options

enum Spriz { Bitter, Aperol, Selet, Cinar, Bianco }

Future<Spriz> _asyncSimpleDialog(
    BuildContext context) async {
  return await showDialog<Spriz>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Select your drink '),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Spriz.Bitter);
              },
              child: const Text('Bitter'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Spriz.Aperol);
              },
              child: const Text('Aperol'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Spriz.Selet);
              },
              child: const Text('Selet'),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, Spriz.Cinar);
              },
              child: const Text('Cinar'),
            ),
          ],
        );
      });
}

// for an async input dialog

Future<String> _asyncInputDialog(
    BuildContext context) async {
  String drinkName = 'prosecchino';
  return showDialog<String>(
    context: context,
    barrierDismissible:
        false, // dialog is dismissible with a tap on the barrier
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Chose your drink'),
        content: new Row(
          children: <Widget>[
            new Expanded(
                child: new TextField(
              autofocus: true,
              decoration: new InputDecoration(
                  labelText: 'drink Name', hintText: 'prosecchino'),
              onChanged: (value) {
                drinkName = value;
              },
            ))
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop(drinkName);
            },
          ),
        ],
      );
    },
  );
}