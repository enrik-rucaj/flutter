# am027_dialogs

In questa esercitazione presentiamo l'uso dei dialogs. Invitiamo ad un ripasso dell'oggetto `Navigator` [qui](https://api.flutter.dev/flutter/widgets/Navigator-class.html).

## Alert Dialog: simple

Per documentazione vedi [qui](https://api.flutter.dev/flutter/material/AlertDialog-class.html), in genere non ha *azioni*. Esempio
```
Future<void> _neverSatisfied() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Rewind and remember'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('You will never be satisfied.'),
              Text('You\’re like me. I’m never satisfied.'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Regret'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
```

## Alert Dialog: Ccnfirm Dialog

Questa volta abbiamo
```
Navigator.of(context).pop(ConfirmAction.ACCEPT);
```

## Simple Dialog: options

Usiamo in questa esercitazione per la prima volta l'oggetto `SimpleDialog` [qui](https://api.flutter.dev/flutter/material/SimpleDialog-class.html) per l'API.
```
Future<void> _askedToLead() async {
  switch (await showDialog<Department>(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        title: const Text('Select assignment'),
        children: <Widget>[
          SimpleDialogOption(
            onPressed: () { Navigator.pop(context, Department.treasury); },
            child: const Text('Treasury department'),
          ),
          SimpleDialogOption(
            onPressed: () { Navigator.pop(context, Department.state); },
            child: const Text('State department'),
          ),
        ],
      );
    }
  )) {
    case Department.treasury:
      // Let's go.
      // ...
    break;
    case Department.state:
      // ...
    break;
  }
}
```
Usiamo `SimpleDialogOption` per le scelte!

# Alert Dialog: text entry

Eccolo qui
```
return AlertDialog(
        title: Text('Chose your drink'),
        content: new Row(
          children: <Widget>[
            new Expanded(
                child: new TextField(
              autofocus: true,
              decoration: new InputDecoration(
                  labelText: 'drink Name', hintText: 'prosecchino'),
              onChanged: (value) {
                drinkName = value;
              },
            ))
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop(drinkName);
            },
          ),
        ],
      );
      ```