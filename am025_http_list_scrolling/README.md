# am025_http_list_scrolling

Il seguente esempio riprende **amo24** completandolo.

## Scrolling

Abbiamo aggiunto uno `ScrollController` in modo da poter caricare dinamicamente altri postPer leAPI [qui](https://api.flutter.dev/flutter/widgets/ScrollController-class.html). Quando raggiongiamo il top ed il buttom coloriamo di giallo un messaggio
```
_scrollListener() {
  if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
    setState(() {
      _message = "reach the bottom";
      _color = Colors.yellow;
    });
    _fetchPosts();  
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
    setState(() {
      _message = "reach the top";
      _color = Colors.yellow;
    });
  }
}

```

## Eccezioni 

Abbiamo inoltre gestito le eccezioni colorando il messaggio un purple chiaro
```
try {
  _fetchPosts();
} catch(e) {
  setState(() {
    _message = "unable to download";
    _color = Colors.purple;
  });
}
```