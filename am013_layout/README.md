# am013_layout

Il progetto ripropone di pari passo il tutorial ufficiale, vedi [qui](https://flutter.dev/docs/development/ui/layout/tutorial). Da segnalare, oltre a quanto evidenziato nei commenti, quanto segue:

- nel metodo `build` si danno due interessanti *buone pratiche* in modo da rendere leffibile il tutto: usare variabili locali di tipo `Widget` e metodi factpry,

- nel codice vediamo come importare immagini, usare icone e soprattutto richiamare allo stile dell'applicazione `Color color = Theme.of(context).primaryColor`.