# am003_named_routes

A new Flutter application: navigare fra schede con nome.

## Getting Started

Questa app segue la precedente, vediamo di confrontarle. Qui:
```
void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In our case, the app will start
    // on the FirstScreen Widget
    initialRoute: '/',
    routes: {
      // When we navigate to the "/" route, build the FirstScreen Widget
      '/': (context) => FirstScreen(),
      // When we navigate to the "/second" route, build the SecondScreen Widget
      '/second': (context) => SecondScreen(),
    },
  ));
}
```
Lì:
```
void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute(),
  ));
}
```
Qui:
```
child: RaisedButton(
  child: Text('Launch screen'),
  onPressed: () {
    Navigator.pushNamed(context, '/second');
  },
),

```
Lì
```
child: RaisedButton(
  child: Text('Open route'),
  onPressed: () {
    Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SecondRoute()),
    );
  },
),
```

## approfondimenti

Per legare meglio questo esempio a quello precedente invitiamo anche alla lettura dell'articolo

[1] "Clean Navigation in Flutter Using Generated Routes Dane Mackier" di Dane Mackie [qui](https://medium.com/flutter-community/clean-navigation-in-flutter-using-generated-routes-891bd6e000df)