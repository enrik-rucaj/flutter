# am004_passing_arguments

In questa applicazione vedremo come gestire il passaggio di informazione fra uno screen ed un altro.

## Getting Started

Qui ampliamo l'esempio precedente. `ModalRoute`, come da [api](https://api.flutter.dev/flutter/widgets/ModalRoute-class.html), è un widget che
> ... blocks interaction with previous routes

Col metodo statico `ModalRoute.of(...)` recuperiamo il più vicine (widget tree, ancestor) `ModalRoute`. Ogni `Route` possiede una proprietà `settings` che a loro volta sono un `RouteSettings` che contengono oltre al possibile nome del *route* ed ad altre informazioni anche un oggetto `arguments`. Come indicato nelle api, [qui](https://docs.flutter.io/flutter/widgets/Navigator/pushNamed.html), possiamo aggiungere un oggetto ad `arguments` per poi recuperarlo, passiamo con
``` dart
Navigator.pushNamed( context, '/second', arguments: bundle);
```
e recuperiamo con
``` dart
final Bundle bundle = ModalRoute.of(context).settings.arguments;
```
un alternativa
``` dart
onGenerateRoute: (settings) {
    if (settings.name == SecondScreen.routeName) {
      // Cast the arguments to the correct type: ScreenArguments.
      final Bundle bundle = settings.arguments;
      return MaterialPageRoute(
        builder: (context) {
          return PassArgumentsScreen(
            ... bundle ...
          );
        },
      );
    }
  },
  ```

