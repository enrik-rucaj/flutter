import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigation with Arguments',
      theme: ThemeData(
          primarySwatch: Colors.red
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        '/second': (context) => SecondScreen(),
      },
    );
  }
}

class HomeScreen extends StatelessWidget {

  final Bundle bundle = Bundle();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                width: 150.0,
                height: 70,
                child: TextField(
                  onChanged: (text) {
                    bundle.message = text;
                    },
                ),
            ),
            RaisedButton(
              child: Text("Send"),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/second',
                  arguments: bundle,
                );
              }
            )
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {

  /*
  final Bundle bundle;

  const SecondScreen({Key key, this.bundle}) : super(key: key);

  SecondScreen(@required this.bundle){}
  */

  @override
  Widget build(BuildContext context) {

    final Bundle bundle = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(bundle.title),
      ),
      body: Center(
        child: Text(
            bundle.message,
          style: TextStyle(fontSize: 40)
        ),
      ),
    );
  }
}

// see Android
class Bundle {
  String title = "the message";
  String message = "";
}