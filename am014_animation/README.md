# am014_animation

Questo esempio riprende il tutorial ... [qui](https://flutter.dev/docs/development/ui/animations/tutorial).

## Costruzione dell'animazione

Per prima cosa si istanzia un `AnimationController`, [qui](https://api.flutter.dev/flutter/animation/AnimationController-class.html) le API, scriviamo
```
AnimationController(duration: const Duration(seconds: 2), vsync: this);
```
il campo `vsync` fa riferimento ad un `Ticker` grazie a cui viene chiamata l'animazione (un suo step) ad ogni frame, in questo caso usiamo il *mixin* `SingleTickerProviderStateMixin` che fornisce il tick solo se l'albero dei widget è abilitato. Con
```
Tween<double>(begin: 0, end: 300)
```
andiamo a definire il secondo parametro dopo il tempo, `Tween` è una *A linear interpolation between a beginning and ending value* (dalle API), quindi avremo una velocità uniforme.

## Animated Widget

Più comodamente si può lavorare con un `AnimatedWidget`
```
class AnimatedLogo extends AnimatedWidget {
  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        child: FlutterLogo(),
      ),
    );
  }
}
```

## Log di animazione e stato

Andando oltre, seguendo il tutorial, ci viene presentato qualche dettagli sugli *stati* diun'animazione, come monitorarlo e come rieseguire un animazione
```
 @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 300).animate(controller)
      // #enddocregion print-state
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      })
      // #docregion print-state
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }
```

## AnimationBuilder

Ecco un'altra possibilità,a  completare il discorso:
```
class GrowTransition extends StatelessWidget {
  GrowTransition({this.child, this.animation});

  final Widget child;
  final Animation<double> animation;

  Widget build(BuildContext context) => Center(
        child: AnimatedBuilder(
            animation: animation,
            builder: (context, child) => Container(
                  height: animation.value,
                  width: animation.value,
                  child: child,
                ),
            child: child),
      );
}
```

## Simultaneo

Si possono dare anche simultanee modifiche
```
class AnimatedLogo extends AnimatedWidget {
  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: FlutterLogo(),
        ),
      ),
    );
  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}
```

