# am032_list

Riprendiamo in questo esempio l'uso delle *key* lavorabdo con una `ReorderableListView`, [qui](https://api.flutter.dev/flutter/material/ReorderableListView-class.html) per le API. Al momento del drag viene chiamato un `ReorderCallback`, [qui](https://api.flutter.dev/flutter/material/ReorderCallback.html) per le api
``` dart
void _handleReorder(int oldIndex, int newIndex) {
    if(newIndex > oldIndex){
      newIndex -= 1;
    }
    final Paint item = paints.removeAt(oldIndex);
    paints.insert(newIndex, item);
  }
```
che è proprio come nelle api. Osserviamo che
``` dart
... paints.removeAt(oldIndex);
```
riduce di un elemento la lista, attenzione quindi all'inserimeto che ne segue: se inseriamo dopo l'estrazione dovremo farlo un passo indietro, abbiamo tolto un elemento aggiornando gli indici della lista, altrimento procederemo ad inserire, gli indici degli elementi che seguono verranno aggiornati sommando 1.

## LocalKey

`ValueKey<T>`, [qui](https://api.flutter.dev/flutter/foundation/ValueKey-class.html) per le api, ci permette di ottenere un valore da un oggetto di tipo `T`, il confronto fra chiavi `==` viene demandato al confronto fra oggetti di tipo `T`.