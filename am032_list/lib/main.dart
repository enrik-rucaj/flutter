import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am032_list'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  double padValue = 0;

  List<Paint> paints = <Paint>[
    Paint(1, 'Red', Colors.red, 0),
    Paint(2, 'Blue', Colors.blue, 1),
    Paint(3, 'Green', Colors.green, 2),
    Paint(4, 'Lime', Colors.lime, 3),
    Paint(5, 'Indigo', Colors.indigo, 4),
    Paint(6, 'Yellow', Colors.yellow, 5)
  ];

  void _handleReorder(int oldIndex, int newIndex) {
    if(newIndex > oldIndex){
      // removing the item at oldIndex will shorten the list by 1
      newIndex -= 1;
    }
    final Paint item = paints.removeAt(oldIndex);
    paints.insert(newIndex, item);
  }
  
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ReorderableListView(
        children: List.generate(paints.length, (index) {
          return ListTile(
            key: ValueKey("value$index"),
            leading: Container(
              width: 130.0,
              alignment: Alignment.center,
              color: paints[index].color,
              child: Text(
                "pos:${index + 1}", 
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 25.0)
              ) 
            ),
            title: Text('ID: ' + paints[index].id.toString()),
            subtitle: Text(paints[index].title),              
          );
        }),
        onReorder: (int oldIndex, int newIndex) {
          setState(() {
            _handleReorder(oldIndex, newIndex);
          });
        },
      ),
    );
  }
}

class Paint {
  final int id;
  final String title;
  final Color color;
  final int position;

  Paint(this.id, this.title, this.color, this.position);
}
