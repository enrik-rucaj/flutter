import 'package:flutter/material.dart';

import 'package:am018_drawing/page_01.dart';
import 'package:am018_drawing/page_02.dart';
import 'package:am018_drawing/page_03.dart';
import 'package:am018_drawing/page_04.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  const MyApp({this.title, Key key }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: "a sketch"),
                Tab(text: "sky"),
                Tab(text: "my chart"),
                Tab(text: "photo"),
              ],
            ),
            title: Text('Drawing'),
          ),
          body: TabBarView(
            children: [
              DrawingPageOne(),
              DrawingPageTwo(),
              DrawingPageThree(),
              DrawingPageFour(),
            ],
          ),
        ),
      ),
    );
  }
}