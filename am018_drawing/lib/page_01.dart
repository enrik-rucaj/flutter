import 'package:flutter/material.dart';

class DrawingPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: MyPaint(),
        child: Center(
          child: Text("Test",
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.cyan, fontSize: 30.0),
          ),
        ),
    );
  }
}

class MyPaint extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {

    // first circle 
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8
      ..color = Colors.amber[700];
    Offset offset = Offset(size.width*0.5, size.height*0.5);
    canvas.drawCircle(offset, size.width*0.25, paint);
    // second circle
    paint.color = Colors.purple[400];
    offset = Offset(size.width*0.30, size.height*0.30);
    canvas.drawCircle(offset, size.width*0.15, paint);
    // third circle
    paint
      ..color = Colors.yellow[600]
      ..style = PaintingStyle.fill;
    offset = Offset(size.width*0.20, size.height*0.20);
    canvas.drawCircle(offset, size.width*0.10, paint);
    // path
    paint 
      ..style = PaintingStyle.stroke
      ..color = Colors.green;
    var path = Path()
    ..moveTo(size.width / 3, size.height * 3 / 4)
    ..lineTo(size.width / 2, size.height * 5 / 6)
    ..lineTo(size.width * 3 / 4, size.height * 4 / 6);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
  
}