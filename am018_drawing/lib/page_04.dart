import 'dart:async';
import 'dart:ui' as ui;
import 'dart:math' as math;
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DrawingPageFour extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<DrawingPageFour> {

  ui.Image image;
  // state initialization
  bool isLoaded;

  @override
	void initState(){
		super.initState();
		isLoaded = false;
  }

  @override
  Widget build(BuildContext context) {
    return _buildImage();
  }

  // asyncwork
  Future<void> _doAsyncWork() async {
    image = await _loadImageAsset("images/photo_01.jpeg");
    setState(() {
      isLoaded = true;
    });
  }

  // widget
  Widget _buildImage() {
    if (this.isLoaded) {
      return new CustomPaint(
        painter: MyPaint(image: image),
      );
    } else {
      _doAsyncWork();
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }
}

class MyPaint extends CustomPainter {
  
  final ui.Image image;

  MyPaint({this.image}) : super();

  @override
  Future paint(Canvas canvas, Size size) async {

    // text
    TextSpan span = TextSpan(
        style: TextStyle(color: Colors.red[800], fontSize: 24.0),
        text: "bla bla bla");
    TextPainter tp = TextPainter(
        text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(canvas, Offset(size.width * 0.35, size.height * 0.75));

    // paint
    final Paint paint = new Paint()
    ..color = Colors.yellow[400]
    ..strokeWidth = 12.0
    ..style = PaintingStyle.stroke;

    // circle
    canvas.drawCircle(Offset(size.width * 0.20, size.height * 0.20), size.width * 0.10, paint);

    // image
    Rect src = const Offset(0.0, 0.0) & const Size(1536.0, 1536.0); // subset of image
    Rect dst = Offset(0, size.height * 0.35) & Size(size.width * 0.5, size.width * 0.5); // destination
    canvas.rotate(-math.pi/6);
    canvas.drawImageRect(image, src, dst, paint);
    
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

// from asset to ui.Image 
Future<ui.Image> _loadImageAsset(String assetName) async {
  final ByteData data = await rootBundle.load(assetName);
  return decodeImageFromList(data.buffer.asUint8List()); // take Uint8List
}