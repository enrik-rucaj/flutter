import 'dart:math';

import 'package:flutter/material.dart';

class DrawingPageThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: MyPaint(),
      child: Center(
          child: Text("My Chart",
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red, fontSize: 30.0),
          ),
        ),
    );
  }
}

class MyPaint extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    _drawRoundedArc(size, canvas, Colors.green, 40, 0, 1);
    _drawRoundedArc(size, canvas, Colors.yellow, 40, 1, 3);
    _drawRoundedArc(size, canvas, Colors.blue, 40, 3, 4);
    _drawRoundedArc(size, canvas, Colors.red, 40, 4, 6);
    _drawRoundedArc(size, canvas, Colors.brown, 40, 6, 6.28);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

void _drawRoundedArc(Size size, Canvas canvas, Color color,  double width, double start, double stop) {
  Paint paint = Paint();
  paint.color = color;
  paint.style = PaintingStyle.stroke;
  double radius = size.width*0.3;
  // draw arc
  paint.strokeWidth = width;
  Offset center = Offset(size.width*0.5, size.height*0.5);
  Rect rect = Rect.fromCircle(center: center, radius: radius);
  canvas.drawArc(rect, start, stop-start, false, paint);
  // draw starting circle
  paint.style = PaintingStyle.fill;
  Offset centerC = center + Offset(radius*cos(start), radius*sin(start));
  canvas.drawCircle(centerC, width*0.5, paint);
  // draw stopping circle
  centerC = center + Offset(radius*cos(stop), radius*sin(stop));
  canvas.drawCircle(centerC, width*0.5, paint);
}
  