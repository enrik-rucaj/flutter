# am018_drawing

A new Flutter project.

## a sketch

`CustomPaint` è un widget, [qui](https://api.flutter.dev/flutter/widgets/CustomPaint-class.html) le API, la cui proprietà `painter` si riferisce ad un `CustomPainter` disegnato dopo il figlio.
> A widget that provides a canvas on which to draw during the paint phase  

`CustomPainter`, [qui](https://api.flutter.dev/flutter/rendering/CustomPainter-class.html) le API, ci permette di impostare il disegno, i metodi più importanti sono  
`paint(Canvas canvas, Size size)` con cui disegnamo sul *canvas*
In genere usiamo
``` dart
@override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
```
possiamo anche avere ad esempio (come in nostro altro esempio)
``` dart
  @override
  bool shouldRepaint(CrossPainter oldDelegate) {
    return oldDelegate._fraction != _fraction;
  }
```
che ci dice quando ridisegnare il tutto (vedi animazioni).  
`Paint` ci permette di preparare il disegno, [qui](https://api.flutter.dev/flutter/painting/painting-library.html) l'API su paint.  
Le coordinate del `canvas` hanno lo zero in alto a sinistra e `width` ed `height` rappresentano la sua misura.

## sky

Esempio preso direttamente da [qui](https://api.flutter.dev/flutter/rendering/CustomPainter-class.html).  

## my chart

Qui disegnamo un diagramma

## photo

Questo è l'esempoio più raffinato. Per prima cosa dagli `asset` recuperiamo un immagine
``` dart
Future<ui.Image> _loadImageAsset(String assetName) async {
  final ByteData data = await rootBundle.load(assetName);
  return decodeImageFromList(data.buffer.asUint8List()); // take Uint8List
}
```
Per prima cosa recuperiamo il `ByteData`
> ... A fixed-length, random-access sequence of bytes that also provides

per un file avremmo potuto scrivere
```
ByteData data = await file.readAsBytes();
```
la proprietà `buffer` è di tipo `ByteBuffer` e va convertita!  
`Image` qui non è il `widget` sebbene sia classe necessaria a costruirlo, [qui](https://api.flutter.dev/flutter/dart-ui/Image-class.html) le API!
L'immagine viene disegnata ed essendo possibilmente non istantaneo il processo abbiamo delegato
``` dart
// asyncwork
Future<void> _doAyncWork() async {
  image = await _loadImageAsset("images/photo_01.jpeg");
    setState(() {
      isLoaded = true;
  });
}
```
In fase di caricamento vedremo la solita rotellina di caricamento
``` dart
// widget
Widget _buildImage() {
  if (this.isLoaded) {
    return new CustomPaint(
      painter: MyPaint(image: image),
    );
  } else {
    _doAyncWork();
    return Center(
    child: CircularProgressIndicator(),
    );
  }
}
```
non possiamo che lavorare con un `StateFullWidget` 
``` dart
@override
void initState(){
  super.initState();
  isLoaded = false;
}
```



