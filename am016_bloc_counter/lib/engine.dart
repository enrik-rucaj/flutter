import 'package:bloc/bloc.dart';

// events
enum CounterEvent { increment, decrement }

// the bloc, the state set is the int set
class CounterBloc extends Bloc<CounterEvent, int> {

  @override
  int get initialState => 0;

  @override
  Stream<int> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.decrement:
        yield state - 1;
        break;
      case CounterEvent.increment:
        yield state + 1;
        break;
    }
  }
}