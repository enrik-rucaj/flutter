# am016_bloc_counter

**Bloc** sta per **Business Logic Component** 
> A Bloc (Business Logic Component) is a component which converts a Stream of incoming Events into a Stream of outgoing States. 

la sua home la troviamo [qui](https://bloclibrary.dev/#/)
Perché usare **Bloc**?
> Bloc makes it easy to separate presentation from business logic, making your code fast, easy to test, and reusable.

![bloc](./bloc.png)

L'esempio qui esposto è tratto da [qui](https://bloclibrary.dev/#/fluttercountertutorial).

## installazione

Come al solito tocchimao il file `pubspec.yamls`
``` dart
dependencies:
  bloc: ^3.0.0
  flutter_bloc: ^3.2.0
```

Seguono i concetti chiave

## events

> Events are the input to a Bloc. They are commonly added in response to user interactions ...

## states

> states are the output of a Bloc and represent a part of your application's state. UI components can be notified of states and redraw portions of themselves based on the current state.

Gli *state* quindi oltre che rappresentare una condizione che dipende dallo stato iniziale e dagli eventi, *income*, ricevuti dal sistema, la **storia** del sistema, è anche l'uscita, *outcome*.


## transition

> The change from one state to another is called a Transition. A Transition consists of the current state, the event, and the next state.

Una *transation* quindi mappa lo stato e l'ingresso attuale nello stato futuro

## la macchina a stati

Nel codice che seguiamo vediamo come lo *stream* di eventi si trasforma in uno *stream* di stati
``` dart
// events
enum CounterEvent { increment, decrement }

// the bloc, the state set is the int set
class CounterBloc extends Bloc<CounterEvent, int> {

  @override
  int get initialState => 0;

  @override
  Stream<int> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.decrement:
        yield state - 1;
        break;
      case CounterEvent.increment:
        yield state + 1;
        break;
    }
  }
}
```
ad ogni **evento** generiamo una **transizione** di **stato** gestita da stream. Per un ripasso sugli *stream* vai [qui](https://dart.dev/articles/libraries/creating-streams)

Lo **stato iniziale** viene definito tramite
``` dart
@override
int get initialState => 0;
```

## widget

Intuitala meccanica entriamo in *Flutter*. Per prima cosa il `BlocProvider`, [qui](https://pub.dev/documentation/flutter_bloc/latest/flutter_bloc/BlocProvider-class.html) per le API, fornisce al `child` un `Bloc`
``` dart
BlocProvider(
  create: (BuildContext context) => BlocA(),
  child: ChildA(),
);
```
e quindi `BlocBuilder`, [qui](https://pub.dev/documentation/flutter_bloc/latest/flutter_bloc/BlocBuilder-class.html) per le API
> BlocBuilder handles building a widget in response to new states. 

``` dart
BlocBuilder<BlocA, BlocAState>(
  builder: (context, state) {
    // return widget here based on BlocA's state
  }
)
```
nel nostro esempio `BlocBuilder<CounterBloc, int>; posizionandoli a cascata si può chiamare
``` dart
final CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);
```
gli event verranno quindi sottomessi allo *stream* con
``` dart
 counterBloc.add(CounterEvent.decrement);
```