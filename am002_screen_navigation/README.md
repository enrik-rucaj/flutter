# am002_screen_navigation

A new Flutter application: navigare fra schede.

## Getting Started

In Android si lavorava con Intent ed Activity, in Flutter si lavora diversamente.
```
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigation Basics',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: FirstRoute(),
    );
  }
}
```
abbiamo impostato (chiamando il costruttore)
```
home: FirstRoute(),
```
Ogni singola schermata è del tipo
```
class FirstRoute extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ...
  }
}
```
La widget ritornata è uno `Scaffold` pensata apposta come *impalcatura* per il Material Design, vedi [qui](https://docs.flutter.io/flutter/material/Scaffold-class.html). Il `RaisedButton` segue le specifiche del Material Design, [qui](https://docs.flutter.io/flutter/material/RaisedButton-class.html) per le API ed esempio da provare. Del `Navigator`,oggetto cruciale, se ne parla [qui](https://flutter.dev/docs/development/ui/navigation) assieme al **routing**. Esso viene definito, [qui](https://docs.flutter.io/flutter/widgets/Navigator-class.html) nelle API, come
> A widget that manages a set of child widgets with a stack discipline.  

quindi richiama allo *stack* su cui venivano posizionate in Android le `Activity` (le schermate). Ogni `Scaffold` messo in cima allo stack avrà sulla barra un bottone per il `pop` quindi 
```
child: RaisedButton(
  onPressed: () {
    Navigator.pop(context);
    },
  child: Text('Go back!'),
)
```
Invece
```
child: RaisedButton(
  child: Text('Open route'),
    onPressed: () {
      Navigator.push(
        context,
        (builder: (context) => SecondRoute()),
        );
    },
)
```
lo è! Il `context` è un `BuildContext` esso è
> A handle to the location of a widget in the widget tree.

vedo [qui](https://docs.flutter.io/flutter/widgets/BuildContext-class.html) le API! Invitiamo a guardare inoltre il metodo `build()`. `MaterialPageRoute` permette di gestire le transizioni, vedi [qui](https://docs.flutter.io/flutter/material/MaterialPageRoute-class.html).
```
child: RaisedButton(
  child: Text('Open route'),
    onPressed: () {
      Navigator.push(
        context,
          MaterialPageRoute(builder: (context) => SecondRoute()),
        );
    },
)
```
I `Widget` sono per flutter non gli oggetti dell'interfaccia grafica ma una descrizione di essi, un **inflater** provvederà alla loro creazione
> Widgets are the central class hierarchy in the Flutter framework. A widget is an immutable description of part of a user interface. Widgets can be inflated into elements, which manage the underlying render tree.

come [qui](https://docs.flutter.io/flutter/widgets/Widget-class.html) è scritto. Per le `StatelessWidget` non è previsto un cambiamento di stato
> The build method of a stateless widget is typically only called in three situations: the first time the widget is inserted in the tree, when the widget's parent changes its configuration, and when an InheritedWidget it depends on changes.

per esse può essere chiamato il metodo `build()`.
> The framework calls this method when this widget is inserted into the tree in a given BuildContext and when the dependencies of this widget change (e.g., an InheritedWidget referenced by this widget changes).

## l'oggetto magico

`Navigation` è proprio l'oggetto magico che ci permette di dimenticare `Activity` e `Fragment`  di `Android`: [qui](https://medium.com/flutter-community/flutter-push-pop-push-1bb718b13c31) per un articolo di approfondimento "Flutter: Push, Pop, Push" di Pooja Bhaumik du Medium.
