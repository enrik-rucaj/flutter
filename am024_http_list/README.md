# am024_http_list

L'esempio scarica dei post usando le api gratuite el [sito](https://jsonplaceholder.typicode.com/). Altra fonte per prenere spunto: [qui](https://medium.com/@diegoveloper/flutter-lets-know-the-scrollcontroller-and-scrollnotification-652b2685a4ac).

## inizializzazione

`initState()` non può essre saincrono, pertanto:
```
@override
void initState() {
  ...
  _fetchPosts();
  super.initState();
}
...
_fetchPosts() async {
  for(int id = 1; id<=4; id++) {
    Post post = await fetchPost(id);
    setState(() {
      _posts.add(post); 
    });
  } 
  ...
}
...
Future<Post> fetchPost(int id) async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/posts/' + id.toString());

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON.
    return Post.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}
```
Il metodo `fetchPost` è come in **http_example**. Data la natura asincrona dei metodi è necessario un`_fetchPosts()` usare anche in fase di inizializzazione `setState().