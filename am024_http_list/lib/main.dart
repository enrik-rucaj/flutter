import 'package:flutter/material.dart';

import 'model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'http',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am024_http_list'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _message = "loaded";
  Color _color = Colors.green; 
  List<Post> _posts =List<Post>();

  @override
  void initState() {
    _color = Colors.orange;
    _message = "loading";
    _fetchPosts();
    super.initState();
  }

  _fetchPosts() async {
    for(int id = 1; id<=4; id++) {
      Post post = await fetchPost(id);
      setState(() {
        _posts.add(post); 
      });
    } 
    setState(() {
      _color = Colors.green;
      _message = "loaded";
    });
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 50.0,
            color: _color,
            child: Center(
              child: Text(_message),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _posts.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text("$index: " + _posts[index].title),
                  subtitle: Text(_posts[index].body)
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}